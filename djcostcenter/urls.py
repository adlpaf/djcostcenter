from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from djcostcenter.core.json_settings import get_settings

settings_json = get_settings()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('djcostcenter.apps.website.urls')),
    url(r'^flow/', include('djcostcenter.apps.flow.urls')),
    url(r'^security/', include('djcostcenter.apps.security.urls'))
]

if settings_json["DEBUG"]:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
