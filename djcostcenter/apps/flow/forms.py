from django import forms
from django.utils import timezone
from .models import Transaction, CostCenter, Account, Category
from django.utils.translation import ugettext as _
from djcostcenter.core.internationalization import DATE_INPUT_FORMATS
from django.db.models import Sum
from decimal import *

class TransactionForm(forms.ModelForm):
    date = forms.DateField(input_formats=DATE_INPUT_FORMATS)

    class Meta:
        model = Transaction
        fields = ['account', 'name', 'category', 'amount', 'cost_center', 'date']

    def clean(self, *args, **kwargs):
    	category_type = self.cleaned_data['category'].type # 0 para egreso y 1 para ingreso

    	if category_type == 0:
	    	allow_overdraft = self.cleaned_data['account'].allow_overdraft

	    	if allow_overdraft == False:
		    	amount = self.cleaned_data['amount']
		    	account_sum = Transaction.objects.filter(account_id=self.cleaned_data['account']).aggregate(suma=Sum('amount'))

		    	if Decimal(account_sum['suma']) < amount:
		    		self.add_error('amount', "El monto no puede exceder la cantidad disponible en la cuenta")
